<?php

use Drupal\search_api\Entity\Index;
use Drupal\search_api\ServerInterface;

/**
 * Implements hook_views_data().
 *
 * @throws \Drupal\search_api\SearchApiException
 */
function search_api_es_views_data() {
  $data = [];

  /** @var \Drupal\search_api\IndexInterface $index */
  foreach (Index::loadMultiple() as $index) {
    $server = $index->getServerInstance();
    // Skip if instance doesn't load.
    if (!$server instanceof ServerInterface) {
      continue;
    }
    // Skip if server not support "search_api_es" backend.
    if ($server->getBackendId() != 'search_api_es') {
      continue;
    }

    $key = 'search_api_index_' . $index->id();
    $data[$key]['spellcheck'] = [
      'title' => t('Did you mean'),
      'help' => t('Shows a possible text request.'),
      'area' => [
        'id' => 'spellcheck',
      ],
    ];
  }

  return $data;
}
