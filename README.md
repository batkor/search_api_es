Support
------------

**Features**
1. `search_api_mlt`.

   Use **More like this** contextual filter.

2. `search_api_random_sort`

   Add **Global:Random** sort in views.

3. `search_api_spellcheck`

   Use **Did you mean**  views area plugin. Worked for `fulltext` type fields

4. `search_api_autocomplete`

   Install **Search API Autocomplete** module.

5. `search_api_facet`
   
   Use as usual.

**Processor**
1. Highlight.

   Enable processor and add **Excerpt** views field.

2. HTML-filter preprocess.

   Enable processor.
