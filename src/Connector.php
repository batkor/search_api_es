<?php

namespace Drupal\search_api_es;

use Drupal\search_api\IndexInterface;
use Drupal\search_api\ServerInterface;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Psr\Log\LoggerInterface;

/**
 * The Elasticsearch connector service.
 */
class Connector {

  /**
   * The initialized clients.
   *
   * @var \Elasticsearch\Client[]
   */
  protected $clients;

  /**
   * The initialized analyzer instance.
   *
   * @var \Drupal\search_api_es\EsAnalyzerInterface[]
   */
  protected $analyzerInstance;

  /**
   * The logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The analyzer plugin manager.
   *
   * @var \Drupal\search_api_es\EsAnalyzerPluginManager
   */
  protected $analyzerPluginManager;

  /**
   * Connector constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger instance.
   * @param \Drupal\search_api_es\EsAnalyzerPluginManager $analyzerPluginManager
   *   The analyzer plugin manager.
   */
  public function __construct(LoggerInterface $logger, EsAnalyzerPluginManager $analyzerPluginManager) {
    $this->logger = $logger;
    $this->analyzerPluginManager = $analyzerPluginManager;
  }

  /**
   * Returns Elasticsearch client.
   *
   * @param \Drupal\search_api\ServerInterface $server
   *   The SearchApi server.
   *
   * @return \Elasticsearch\Client
   *   The Elasticsearch client.
   */
  public function getClient(ServerInterface $server): Client {

    $config = $server->getBackendConfig();

    if (!isset($this->clients[$server->id()])) {
      $client_builder = ClientBuilder::create();
      $client_builder->setHosts([$config['server_url']]);
      if (array_key_exists('base_auth', $config) && $config['base_auth']) {
        $client_builder->setBasicAuthentication($config['username'], $config['password']);
      }

      $this->clients[$server->id()] = $client_builder->build();
    }

    return $this->clients[$server->id()];
  }

  /**
   * Build base parameters for elsaticsearch index.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The SearchApi index.
   *
   * @return array
   *   The parameters for query in elasticsearch.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *
   * @see \Elasticsearch\Namespaces\IndicesNamespace::create
   */
  public function buildParams(IndexInterface $index): array {
    $settings = $index->getThirdPartySettings('search_api_es');

    $enable_analyzers = isset($settings['es_analyzer']) ? $settings['es_analyzer'] : [];
    $enable_analyzers = array_filter($enable_analyzers);
    $analyzers = [];
    $filters = [];
    $char_filters = [];

    foreach ($enable_analyzers as $analyzer_plugin_id) {
      $analyzers += $this->getAnalyzer($analyzer_plugin_id);
      $filters += $this->getFilter($analyzer_plugin_id);
      $char_filters += $this->getCharFilter($analyzer_plugin_id);
    }

    $params = [
      'index' => $index->id(),
      'body' => [
        'settings' => [
          'number_of_shards' => isset($settings['number_of_shards']) ? $settings['number_of_shards'] : 1,
          'number_of_replicas' => isset($settings['number_of_replicas']) ? $settings['number_of_replicas'] : 2,
          'analysis' => [
            'analyzer' => $analyzers,
            'filter' => $filters,
            'char_filter' => $char_filters,
          ],
        ],
      ],
    ];


    return $params;
  }

  /**
   * Returns char filter from plugin.
   *
   * @param string $plugin_id
   *   The analyzer plugin id.
   *
   * @return array
   *   The "char_filter" settings.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getCharFilter(string $plugin_id): array {
    return $this->getInstance($plugin_id)->charFilter();
  }

  /**
   * Returns filter from plugin.
   *
   * @param string $plugin_id
   *   The analyzer plugin id.
   *
   * @return array
   *   The filter settings.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getFilter(string $plugin_id): array {
    return $this->getInstance($plugin_id)->filter();
  }

  /**
   * Returns analyzer from plugin.
   *
   * @param string $plugin_id
   *   The analyzer plugin id.
   *
   * @return array
   *   The analyzer settings.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getAnalyzer(string $plugin_id): array {
    return $this->getInstance($plugin_id)->analyzer();
  }

  /**
   * Returns analyzer plugin instance.
   *
   * @param string $plugin_id
   *   The analyzer plugin id.
   *
   * @return \Drupal\search_api_es\EsAnalyzerInterface
   *   The plugin instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getInstance(string $plugin_id) {

    if (isset($this->analyzerInstance[$plugin_id])) {
      return $this->analyzerInstance[$plugin_id];
    }
    $this->analyzerInstance[$plugin_id] = $this
      ->analyzerPluginManager
      ->createInstance($plugin_id);

    return $this->analyzerInstance[$plugin_id];
  }

}
