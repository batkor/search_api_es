<?php

namespace Drupal\search_api_es;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\YamlDiscovery;
use Drupal\Core\Plugin\Factory\ContainerFactory;

/**
 * Defines a plugin manager to deal with search_api_es.index_configs.
 *
 * Modules can define elasticsearch analyzers in a MODULE_NAME.es_analyzer.yml.
 * File contained in the module's base directory.
 * Each elasticsearch analyzers has the following structure:
 *
 * @code
 *   MACHINE_NAME:
 *     label: STRING
 *     description: STRING
 *     analyzer: object
 *     filter: object
 *     char_filter: object
 * @endcode
 *
 * @see \Drupal\search_api_es\EsAnalyzerDefault
 * @see \Drupal\search_api_es\EsAnalyzerInterface
 * @see plugin_api
 */
class EsAnalyzerPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  protected $defaults = [
    // The elasticsearch analyzer id.
    'id' => '',
    // The elasticsearch analyzer label.
    'label' => '',
    // The elasticsearch analyzer description.
    'description' => '',
    // The custom elasticsearch analyzer.
    'analyzer' => '',
    // The custom elasticsearch filter use in analyzer.
    'filter' => '',
    // The custom elasticsearch char_filter use in analyzer.
    'char_filter' => '',
    // Default plugin class.
    'class' => 'Drupal\search_api_es\EsAnalyzerDefault',
  ];

  /**
   * Constructs SearchApiEsIndexConfigPluginManager object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   */
  public function __construct(ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend) {
    $this->factory = new ContainerFactory($this);
    $this->moduleHandler = $module_handler;
    $this->setCacheBackend($cache_backend, 'search_api_es.analyzer');
  }

  /**
   * Returns short data all defined analyzers.
   *
   * @return array
   *   The short data all defined analyzers.
   */
  public function getAnalyzersForOptions() {
    $analyzers = [];
    foreach ($this->getDefinitions() as $plugin_id => $def) {
      $analyzers[$plugin_id] = [
        'label' => $def['label'],
        'description' => $def['description'],
      ];
    }

    return $analyzers;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery() {
    if (!isset($this->discovery)) {
      $this->discovery = new YamlDiscovery('es_analyzer', $this->moduleHandler->getModuleDirectories());
      $this->discovery->addTranslatableProperty('label', 'label_context');
      $this->discovery->addTranslatableProperty('description', 'description_context');
    }
    return $this->discovery;
  }

}
