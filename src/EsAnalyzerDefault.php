<?php

namespace Drupal\search_api_es;

use Drupal\Core\Plugin\PluginBase;

/**
 * Default class used for search_api_es.index_configs plugins.
 */
class EsAnalyzerDefault extends PluginBase implements EsAnalyzerInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function analyzer() {
    return isset($this->pluginDefinition['analyzer']) ? $this->pluginDefinition['analyzer'] : [];
  }

  /**
   * {@inheritdoc}
   */
  public function filter() {
    return isset($this->pluginDefinition['filter']) ? $this->pluginDefinition['filter'] : [];
  }

  /**
   * {@inheritdoc}
   */
  public function charFilter() {
    return isset($this->pluginDefinition['char_filter']) ? $this->pluginDefinition['char_filter'] : [];
  }
}
