<?php

namespace Drupal\search_api_es\Plugin\views\argument;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Plugin\views\argument\SearchApiMoreLikeThis;

/**
 * This plugin override "search_api_more_like_this" argument plugin.
 *
 * @package Drupal\search_api_es\Plugin\views\argument
 */
class SearchApiEsMoreLikeThis extends SearchApiMoreLikeThis {

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    if ($form['fields']['#type'] != 'select') {
      return;
    }

    $index = Index::load(substr($this->table, 17));
    $fields = ['search_api_language' => 'Language'];

    /** @var \Drupal\search_api\Item\FieldInterface $field */
    foreach ($index->getFields() as $key => $field) {
      $config = $field->getConfiguration();
      if (!isset($config['search_api_es'])) {
        continue;
      }
      if ($config['search_api_es']['term_vector'] != 'no') {
        $fields[$key] = $field->getLabel();
      }
    }

    $form['fields']['#options'] = $fields;
  }

}
