<?php

namespace Drupal\search_api_es\Plugin\views\area;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\search_api\Plugin\views\filter\SearchApiFulltext;
use Drupal\search_api\Plugin\views\query\SearchApiQuery;
use Drupal\search_api\Query\QueryInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Views area handler to show a possible text request.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("spellcheck")
 */
class Spellcheck extends AreaPluginBase {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatch
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CurrentRouteMatch $currentRouteMatch) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->routeMatch = $currentRouteMatch->getCurrentRouteMatch();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['content'] = [
      'default' => $this->t('Did you mean: @corrected_text'),
    ];
    $options['max_errors'] = ['default' => 3];
    $options['min_word_length'] = ['default' => 3];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['content'] = [
      '#title' => $this->t('Display'),
      '#type' => 'textarea',
      '#rows' => 3,
      '#default_value' => $this->options['content'],
      '#description' => $this->t('You may use HTML code in this field. Token: @tokens', [
        '@tokens' => '<code>@corrected_text</code>',
      ]),
    ];
    $form['max_errors'] = [
      '#title' => $this->t('Max errors in word'),
      '#type' => 'number',
      '#default_value' => $this->options['max_errors'],
      '#description' => $this->t('The maximum percentage of the terms considered to be misspellings in order to form a correction.'),
    ];

    $form['min_word_length'] = [
      '#title' => $this->t('The minimum length text'),
      '#type' => 'number',
      '#default_value' => $this->options['min_word_length'],
      '#description' => $this->t('The minimum length a suggest text term must have in order to be included.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->query->setOption('search_api_spellcheck', [
      'max_errors' => $this->options['max_errors'],
      'min_word_length' => $this->options['min_word_length'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {

    // Skip if query not instance SearchApiQuery.
    if (!$this->query instanceof SearchApiQuery) {
      return [];
    }

    $query = $this->query->getSearchApiQuery();

    // Skip if query not exist.
    if (!$query instanceof QueryInterface) {
      return [];
    }

    $raw_response = $query->getResults()->getExtraData('search_api_es_response');

    // Skip if "suggest" parameter not exist.
    if (!isset($raw_response['suggest'])) {
      return [];
    }

    foreach ($raw_response['suggest'] as $suggest_key => $suggest_item) {
      if (stripos($suggest_key, 'search_api_spellcheck') === FALSE) {
        continue;
      }
      foreach ($suggest_item as $suggest_item_response) {
        if (empty($suggest_item_response['options'])) {
          continue;
        }
        $text = reset($suggest_item_response['options'])['text'];
        break 2;
      }
    }

    if (!isset($text)) {
      return [];
    }

    $filter_key = $this->getFilterIdentifier();
    // Skip if filter identifier not exist.
    if (!$filter_key) {
      return [];
    }

    $replacement = Link::createFromRoute($text, $this->routeMatch->getRouteName(), [], ['query' => [$filter_key => $text]])->toString();
    return [
      '#markup' => Xss::filterAdmin(str_replace('@corrected_text', $replacement, $this->options['content'])),
    ];
  }

  /**
   * Returns fulltext filter identifier or null if filter not found.
   *
   * @return string|null
   *   The fulltext keys.
   */
  protected function getFilterIdentifier() {
    foreach ($this->view->filter as $filter) {
      if (!$filter instanceof SearchApiFulltext) {
        continue;
      }
      if (isset($filter->options['expose']['identifier'])) {
        $key = $filter->options['expose']['identifier'];
        break;
      }
    }

    return $key ?: NULL;
  }

}
