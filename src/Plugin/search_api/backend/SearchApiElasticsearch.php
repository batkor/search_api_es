<?php

namespace Drupal\search_api_es\Plugin\search_api\backend;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\Backend\BackendPluginBase;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Plugin\search_api\data_type\value\TextValue;
use Drupal\search_api\Plugin\search_api\data_type\value\TextValueInterface;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\Query\ResultSetInterface;
use Drupal\search_api_autocomplete\AutocompleteBackendInterface;
use Drupal\search_api_autocomplete\Plugin\search_api_autocomplete\suggester\Server;
use Drupal\search_api_autocomplete\SearchInterface;
use Drupal\search_api_autocomplete\Suggestion\SuggestionFactory;
use Drupal\search_api_es\Connector;
use Drupal\search_api_es\Utility\Converter\FieldMapping;
use Drupal\search_api_es\Utility\Converter\QueryConverter;
use Elasticsearch\Common\Exceptions\ElasticsearchException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Elasticsearch backend for search api.
 *
 * @SearchApiBackend(
 *   id = "search_api_es",
 *   label = @Translation("Elasticsearch"),
 *   description = @Translation("Index items using an Elasticsearch search server.")
 * )
 */
class SearchApiElasticsearch extends BackendPluginBase implements PluginFormInterface, AutocompleteBackendInterface {

  /**
   * The Elasticsearch connector service.
   *
   * @var \Drupal\search_api_es\Connector
   */
  protected $connector;

  /**
   * The logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    Connector $connector,
    LoggerInterface $logger,
    Messenger $messenger
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->connector = $connector;
    $this->logger = $logger;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('search_api_es.connector'),
      $container->get('logger.channel.search_api_es'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function addIndex(IndexInterface $index) {
    try {
      $this
        ->connector
        ->getClient($this->getServer())
        ->indices()
        ->create($this->connector->buildParams($index));
    } catch (ElasticsearchException $e) {
      $this->messenger->addError($e->getMessage());
      $this->logger->error($e);
      $this->messenger->addWarning($this->t('Not create elasticsearch index.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function updateIndex(IndexInterface $index) {
    $client = $this
      ->connector
      ->getClient($this->getServer());

    // Remove old index.
    if ($client->indices()->exists(['index' => $index->id()])) {
      $client->indices()->delete(['index' => $index->id()]);
    }

    // Create new index. Add field mappings and settings.
    $params = $this->connector->buildParams($index);
    // Add base mapping settings.
    $params['body']['mappings'] = FieldMapping::create($index)->get();

    try {
      $client->indices()->create($params);
    }
    catch (ElasticsearchException $e) {
      $this->messenger->addError($e->getMessage());
      $this->logger->error($e);
    }
    $index->reindex();
  }

  /**
   * {@inheritdoc}
   */
  public function removeIndex($index) {
    $client = $this->connector->getClient($this->getServer());
    if ($client->indices()->exists(['index' => $index->id()])) {
      $client->indices()->delete(['index' => $index->id()]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function indexItems(IndexInterface $index, array $items) {
    if (empty($items)) {
      return [];
    }

    $client = $this->connector->getClient($this->getServer());
    foreach ($items as $id => $item) {
      $hit = [];
      $params['body'][] = [
        'index' => [
          '_index' => $index->id(),
          '_id' => $id,
        ],
      ];
      $hit['search_api_language'] = $item->getLanguage();
      $item_fields = $item->getFields();
      foreach ($item_fields as $field_name => $item_field) {
        foreach ($item_field->getValues() as $value) {
          if ($value instanceof TextValueInterface) {
            $tokens = $value->getTokens();
            foreach ($tokens as $token) {
              $token_field = FieldMapping::getTokenField($field_name, $token->getBoost());
              $hit[$token_field][] = $token->getText();
            }
          }
          $hit[$field_name][] = $value instanceof TextValue ? $value->getText() : $value;
        }
      }
      $params['body'][] = $hit;
    }

    try {
      $client->bulk($params);
    }
    catch (ElasticsearchException $e) {
      $this->messenger->addError($e->getMessage());
      $this->logger->error($e);
    }

    return array_keys($items);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteItems(IndexInterface $index, array $item_ids) {
    vd($item_ids, __FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAllIndexItems(IndexInterface $index, $datasource_id = NULL) {
    $client = $this->connector->getClient($this->getServer());
    if ($client->indices()->exists(['index' => $index->id()])) {
      $client->indices()->delete(['index' => $index->id()]);
    }
    $client->indices()->create(['index' => $index->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function search(QueryInterface $query) {
    $results = $query->getResults();

    $params = ['index' => $query->getIndex()->id()];
    $client = $this
      ->connector
      ->getClient($this->getServer());

    // Set only result count if exist 'server_index_status' tag.
    if ($query->hasTag('server_index_status')) {
      // @see search_api_preprocess_search_api_index
      try {
        $response = $client->search($params);
        $results->setResultCount($response['hits']['total']['value']);
      }
      catch (ElasticsearchException $e) {
        $results->setResultCount(0);
        $this->messenger->addError($e->getMessage());
        $this->logger->error($e);
      }

      return;
    }

    $params['body'] = QueryConverter::create($query)->toArray();
    // Search in elasticsearch.
    $hits = [];
    try {
      $response = $client->search($params);
      $results->setExtraData('search_api_es_response', $response);
      $results->setResultCount($response['hits']['total']['value']);
      $hits = $response['hits']['hits'];
    }
    catch (\Exception $e) {
      $results->setResultCount(0);
      $response['hits']['hits'] = [];
      $this->messenger->addError($e->getMessage());
      $this->logger->error($e);
    }

    // Add elsaticsearch result to Query result.
    foreach ($hits as $hit) {
      /** @var \Drupal\search_api\Utility\FieldsHelper $field_helper */
      $field_helper = \Drupal::service('search_api.fields_helper');
      $result_item = $field_helper->createItem($query->getIndex(), $hit['_id']);
      $index_fields = $query->getIndex()->getFields();
      foreach ($index_fields as $index_field) {
        $field = clone $index_field;
        // Skip if index field_id not found in elasticsearch.
        if (!isset($hit['_source'][$field->getFieldIdentifier()])) {
          continue;
        }

        if (is_array($hit['_source'][$field->getFieldIdentifier()])) {
          foreach ($hit['_source'][$field->getFieldIdentifier()] as $item) {
            $field->addValue($item);
          }
        }
        else {
          $field->addValue($hit['_source'][$field->getFieldIdentifier()]);
        }

        // Add highlight results.
        if (isset($hit['highlight'][$field->getFieldIdentifier()])) {
          // Add result if not empty.
          if (!empty($hit['highlight'][$field->getFieldIdentifier()])) {
            $excerpt = reset($hit['highlight'][$field->getFieldIdentifier()]);
            $result_item->setExcerpt($excerpt);
          }
        }
        // Set language.
        $result_item->setLanguage($hit['_source']['search_api_language']);

        $result_item->setField($field->getFieldIdentifier(), $field);
      }

      $results->addResultItem($result_item);
    }
    $this->extractFacets($results, $query->getOption('search_api_facets'));
  }

  /**
   * Add facets search result to extra data in query result.
   *
   * @param \Drupal\search_api\Query\ResultSetInterface $results
   *   The query result.
   * @param array $facets
   *   The facets properties.
   */
  protected function extractFacets(ResultSetInterface $results, array $facets = NULL) {
    $response = $results->getExtraData('search_api_es_response');
    if (!$response || !$facets) {
      return;
    }

    $extra = [];
    foreach ($facets as $key => $facet) {
      $buckets = $response['aggregations'][$key]['buckets'];
      foreach ($buckets as $bucket) {
        $extra[$key][] = [
          'count' => $bucket['doc_count'],
          'filter' => $bucket['key'],
        ];
      }
    }

    $results->setExtraData('search_api_facets', $extra);
  }

  /**
   * {@inheritdoc}
   */
  public function getAutocompleteSuggestions(QueryInterface $query, SearchInterface $search, $incomplete_key, $user_input) {
    $min_length = $search->getOption('min_length');
    if (strlen($user_input) <= $min_length) {
      return [];
    }

    $params = ['index' => $query->getIndex()->id()];
    $client = $this
      ->connector
      ->getClient($this->getServer());

    $server = $search->getSuggester('server');
    if (!$server instanceof Server) {
      return [];
    }
    $server_config = $server->getConfiguration();
    if (!isset($server_config['fields']) || !empty($server_config['fields'])) {
      $query->setFulltextFields($server_config['fields']);
    }
    else {
      $query->setFulltextFields($query->getIndex()->getFulltextFields());
    }

    $size = $search->getOption('limit') ?: 10;
    $params['body']['suggest'] = QueryConverter::create($query)
      ->buildSuggest('search_api_autocomplete_suggest_', $user_input, $size, 3, $min_length);

    try {
      $response = $client->search($params);
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());

      return [];
    }

    // Skip if "suggest" parameter not exist.
    if (!isset($response['suggest'])) {
      return [];
    }

    $suggest_texts = [];
    foreach ($response['suggest'] as $suggest_key => $suggest_item) {
      if (stripos($suggest_key, 'search_api_autocomplete_suggest') === FALSE) {
        continue;
      }

      foreach ($suggest_item as $suggest_item_response) {
        if (empty($suggest_item_response['options'])) {
          continue;
        }
        foreach ($suggest_item_response['options'] as $option) {
          $suggest_texts[] = $option['text'];
        }
      }
    }
    $suggestion = [];
    $suggestion_factory = new SuggestionFactory($user_input);
    foreach ($suggest_texts as $suggest_text) {
      $suggestion[] = $suggestion_factory->createFromSuggestedKeys($suggest_text);
    }

    return $suggestion;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'server_url' => '',
      'base_auth' => FALSE,
      'username' => '',
      'password' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['server_url'] = [
      '#type' => 'textfield',
      '#title' => t('Server URL'),
      '#default_value' => $this->configuration['server_url'],
      '#description' => t('URL and port of a server (node) in the cluster. Please, always enter the port even if it is default one.  Nodes will be automatically discovered. Examples: http://localhost:9200 or https://localhost:443.'),
      '#required' => TRUE,
    ];

    $form['base_auth'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('HTTP Basic Authentication'),
      '#default_value' => $this->configuration['base_auth'],
    ];

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $this->configuration['username'],
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="backend_config[base_auth]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('If this field is left blank and the HTTP username is filled out, the current password will not be changed.'),
      '#states' => [
        'visible' => [
          ':input[name="backend_config[base_auth]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $server_url = $form_state->getValue('server_url');
    if (!UrlHelper::isValid($server_url)) {
      $form_state->setErrorByName('server_url', $this->t('Is not valid server url'));
    }
    if (!UrlHelper::isExternal($server_url)) {
      $form_state->setErrorByName('server_url', $this->t('Please add protocol. Example http://'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // TODO: Implement submitConfigurationForm() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedFeatures() {
    return [
      'search_api_mlt',
      'search_api_random_sort',
      'search_api_spellcheck',
      'search_api_autocomplete',
      'search_api_facets',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewSettings() {
    $info[] = [
      'label' => $this->t('Url to Elasticsearch'),
      'info' => $this->configuration['server_url'],
    ];

    try {
      $client = $this
        ->connector
        ->getClient($this->getServer());
      $health = $client->cluster()->health();
      $client_info = $client->info();

      $info[] = [
        'label' => $this->t('Elasticsearch version'),
        'info' => $client_info['version']['number'],
      ];

      $info[] = [
        'label' => $this->t('Cluster name'),
        'info' => $health['cluster_name'],
      ];

      $info[] = [
        'label' => $this->t('Health status of the cluster'),
        'info' => $health['status'],
      ];

    }
    catch (\Exception $e) {
      $info[] = [
        'label' => $this->t('Error connecting server:'),
        'info' => $e->getMessage(),
      ];
      return $info;
    }

    return $info;
  }

}
