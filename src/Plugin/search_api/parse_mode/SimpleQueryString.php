<?php

namespace Drupal\search_api_es\Plugin\search_api\parse_mode;

use Drupal\search_api\Plugin\search_api\parse_mode\Direct;

/**
 * Represents a parse mode that just passes the user input on as-is.
 *
 * @SearchApiParseMode(
 *   id = "simple_query_string",
 *   label = @Translation("Simple query string"),
 *   description = @Translation("Returns documents based on a provided query string, using a parser with a limited but fault-tolerant syntax."),
 * )
 */
class SimpleQueryString extends Direct {

}
