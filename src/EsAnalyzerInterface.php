<?php

namespace Drupal\search_api_es;

/**
 * Interface for search_api_es.index_config plugins.
 */
interface EsAnalyzerInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Returns the plugin analyzer settings.
   *
   * @return array
   *   The analyzer settings.
   */
  public function analyzer();

  /**
   * Returns the plugin filter settings.
   *
   * @return array
   *   The filter settings.
   */
  public function filter();

  /**
   * Returns the plugin "char_filter" settings.
   *
   * @return array
   *   The "char_filter" settings.
   */
  public function charFilter();

}
