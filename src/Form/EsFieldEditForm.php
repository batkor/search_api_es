<?php

namespace Drupal\search_api_es\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\search_api\Item\FieldInterface;
use Drupal\search_api\UnsavedConfigurationInterface;

/**
 * The field config form.
 *
 * @package Drupal\search_api_es\Form
 */
class EsFieldEditForm extends FormBase {

  /**
   * The unsaved index configuration.
   *
   * @var \Drupal\search_api\UnsavedConfigurationInterface
   */
  protected $index;

  /**
   * The SearchApi fields in index.
   *
   * @var \Drupal\search_api\Item\FieldInterface
   */
  protected $field;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'es_field_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, UnsavedConfigurationInterface $search_api_index = NULL, $field_id = NULL) {
    $this->index = $search_api_index;

    $fields = $this->index->getFields();

    $this->field = isset($fields[$field_id]) ? $fields[$field_id] : NULL;
    if (!$this->field instanceof FieldInterface) {
      $form['warning'] = ['#markup' => $this->t('Field @field_id not found', ['@field_id' => $field_id])];

      return $form;
    }

    $config = $this->field->getConfiguration();
    $config = isset($config['search_api_es']) ? $config['search_api_es'] : [];

    $form['boost'] = [
      '#type' => 'number',
      '#title' => $this->t('Boost'),
      '#description' => $this->link('https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-boost.html'),
      '#default_value' => isset($config['boost']) ? $config['boost'] : 1.0,
      '#step' => 0.1,
      '#min' => 0,
    ];

    $form['spellcheck'] = [
      '#type' => 'checkbox',
      '#title' => t('Use spell check'),
      '#description' => $this->link('https://www.elastic.co/guide/en/elasticsearch/reference/current/search-suggesters.html'),
      '#default_value' => isset($config['spellcheck']) ? $config['spellcheck'] : FALSE,
    ];

    $form['index_phrases'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Index phrases'),
      '#description' => $this->link('https://www.elastic.co/guide/en/elasticsearch/reference/current/index-phrases.html'),
      '#defaulr_value' => isset($config['index_phrases']) ? $config['index_phrases'] : FALSE,
      '#access' => $this->isFulltext(),
    ];

    $form['index_prefixes'] = [
      '#type' => 'details',
      '#title' => $this->t('Index prefixes'),
      '#open' => TRUE,
      '#description' => $this->link('https://www.elastic.co/guide/en/elasticsearch/reference/current/index-prefixes.html'),
      '#access' => $this->isFulltext(),
      '#tree' => TRUE,
      'min_chars' => [
        '#type' => 'number',
        '#title' => $this->t('Min chars'),
        '#default_value' => isset($config['index_prefixes']['min_chars']) ? $config['index_prefixes']['min_chars'] : 2,
        '#min' => 0,
      ],
      'max_chars' => [
        '#type' => 'number',
        '#title' => $this->t('Max chars'),
        '#default_value' => isset($config['index_prefixes']['max_chars']) ? $config['index_prefixes']['max_chars'] : 5,
        '#min' => 0,
        '#max' => 20,
      ],
    ];

    $form['similarity'] = [
      '#type' => 'select',
      '#title' => $this->t('Similarity'),
      '#options' => [
        'BM25' => 'BM25',
        'classic' => 'classic',
        'boolean' => 'boolean',
      ],
      '#description' => $this->link('https://www.elastic.co/guide/en/elasticsearch/reference/current/similarity.html'),
      '#default_value' => isset($config['similarity']) ? $config['similarity'] : 'BM25',
      '#access' => $this->isFulltext(),
    ];

    $path = 'https://www.elastic.co/guide/en/elasticsearch/reference/current/term-vector.html';
    $form['term_vector'] = [
      '#type' => 'select',
      '#title' => $this->t('Term vector'),
      '#options' => [
        'no' => 'no',
        'yes' => 'yes',
        'with_positions' => 'with_positions',
        'with_offsets' => 'with_offsets',
        'with_positions_offsets' => 'with_positions_offsets',
        'with_positions_payloads' => 'with_positions_payloads',
        'with_positions_offsets_payloads' => 'with_positions_offsets_payloads',
      ],
      '#description' => $this->link($path, 'Add support MLT query'),
      '#default_value' => isset($config['term_vector']) ? $config['term_vector'] : 'no',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#ajax' => [
        'callback' => '::submitForm',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fields = $this->index->getFields();
    $config = $this->field->getConfiguration();

    $config['search_api_es']['boost'] = $form_state->getValue('boost', 1);
    $config['search_api_es']['suggestion'] = $form_state->getValue('suggestion', FALSE);
    $config['search_api_es']['index_phrases'] = $form_state->getValue('index_phrases', FALSE);
    $config['search_api_es']['index_prefixes'] = $form_state->getValue('index_prefixes', [
      'min_chars' => 2,
      'max_chars' => 5,
    ]);
    $config['search_api_es']['similarity'] = $form_state->getValue('similarity', 'BM25');
    $config['search_api_es']['term_vector'] = $form_state->getValue('term_vector', 'no');

    $this->field->setConfiguration($config);
    $fields[$this->field->getFieldIdentifier()] = $this->field;
    $this->index->setFields($fields);
    $this->index->savePermanent();

    return $this->closeDialog();
  }

  /**
   * Returns ajax response command for close dialog.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The AjaxResponse for close dialog window.
   */
  protected function closeDialog() {
    $response = new AjaxResponse();
    $response->addCommand(new CloseDialogCommand('#drupal-off-canvas'));

    return $response;
  }

  /**
   * Returns TRUE if field in fulltext else FALSE.
   *
   * @return bool
   *   The fulltext field status.
   */
  protected function isFulltext() {
    return in_array($this->field->getFieldIdentifier(), $this->index->getFulltextFields());
  }

  /**
   * Returns link.
   *
   * @param string $path
   *   The absolute path for link.
   * @param string $title
   *   The link title.
   *
   * @return \Drupal\Core\Link
   *   The link object.
   */
  protected function link($path, $title = 'More information') {
    return Link::fromTextAndUrl($title, Url::fromUri($path, ['attributes' => ['target' => '_blank']]));
  }

}
