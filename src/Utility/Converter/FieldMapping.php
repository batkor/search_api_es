<?php

namespace Drupal\search_api_es\Utility\Converter;

use Drupal\search_api\IndexInterface;
use Drupal\search_api\SearchApiException;

/**
 * Defines object for create elasticsearch map from SearchApi index.
 */
class FieldMapping {

  /**
   * The SearchApi index.
   *
   * @var \Drupal\search_api\IndexInterface
   */
  protected $index;

  /**
   * Returns instance FieldMapping object.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The SearchApi index.
   *
   * @return static
   *   The instance FieldMapping object.
   */
  public static function create(IndexInterface $index) {
    return new static($index);
  }

  /**
   * FieldMapping object constructor.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The SearchApi index.
   */
  public function __construct(IndexInterface $index) {
    $this->index = $index;
  }

  /**
   * Returns elasticsearch field map.
   *
   * @return array
   *   The field map properties.
   */
  public function get() {
    return [
      '_source' => [
        'enabled' => TRUE,
      ],
      'properties' => $this->properties(),
    ];
  }

  /**
   * Returns fields properties for elasticsearch..
   *
   * @return array
   *   The fields properties.
   */
  protected function properties(): array {
    // Add language.
    $properties = [
      'search_api_language' => [
        'type' => $this->associateTypes()['string'],
        'fields' => [
          'mlt' => [
            "type" => "text",
            "analyzer" => "keyword",
            'term_vector' => 'yes',
          ],
        ],
      ],
    ];

    $fulltext_fields = $this->index->getFulltextFields();
    $analyzers = $this->enabledAnalyzers();
    foreach ($this->index->getFields() as $field_name => $field) {
      $config = $field->getConfiguration();

      $config = isset($config['search_api_es']) ? $config['search_api_es'] : [];
      // Add type.
      $properties[$field_name]['type'] = $this->fieldType($field->getType());
      // Add boost.
      $properties[$field_name]['boost'] = isset($config['boost']) ? (float) $config['boost'] : 1.0;

      // Add suggestion.
      if (isset($config['suggestion']) && $config['suggestion']) {
        $properties[$field_name]['fields']['suggestion'] = [
          "type" => "completion",
        ];
      }
      // Add mlt support.
      if (isset($config['term_vector']) && $config['term_vector'] != 'no') {
        $properties[$field_name]['fields']['mlt'] = [
          "type" => "text",
          "analyzer" => "keyword",
          'term_vector' => $config['term_vector'],
        ];
      }

      // Add copy to.
      if (isset($config['copy_to']) && !empty($config['copy_to'])) {
        $properties[$field_name]['copy_to'] = $config['copy_to'];
      }

      // Special options for fulltext fields.
      if (in_array($field_name, $fulltext_fields)) {
        // Add support analizer fields.
        foreach ($analyzers as $enable_analyzer) {
          $properties[$field_name]['fields'][$enable_analyzer] = [
            'type' => $this->fieldType($field->getType()),
            'analyzer' => $enable_analyzer,
          ];
        }

        // Add index_phrases.
        $properties[$field_name]['index_phrases'] = isset($config['index_phrases']) ? (bool) $config['index_phrases'] : FALSE;

        // Add index_prefixes.
        $properties[$field_name]['index_prefixes'] = isset($config['index_prefixes']) ? $config['index_prefixes'] : [
          'min_chars' => 2,
          'max_chars' => 5,
        ];

        // Add index_phrases.
        $properties[$field_name]['similarity'] = isset($config['similarity']) ? $config['similarity'] : 'BM25';

      }
    }

    // Add boost html tags.
    try {
      $html_filter = $this->index->getProcessor('html_filter')->getConfiguration();
      if ($html_filter) {
        foreach ($html_filter['fields'] as $field_name) {
          // Skip if field not found.
          if (!array_key_exists($field_name, $properties)) {
            continue;
          }

          // Add default boost 1.
          $properties["{$field_name}_1"] = $properties[$field_name];
          $properties["{$field_name}_1"]['boost'] = 1.0;
          // Add second boost values.
          foreach ($html_filter['tags'] as $boost) {
            $convert_field = self::getTokenField($field_name, $boost);
            if (!isset($properties[$convert_field])) {
              $properties[$convert_field] = $properties[$field_name];
              $properties[$convert_field]['boost'] = (float) $boost;
            }
          }
          // Disable index for source field.
          $properties[$field_name]['index'] = FALSE;
          // Remove "index_prefixes".
          unset($properties[$field_name]['index_prefixes']);
        }

      }
    }
    catch (SearchApiException $e) {
      // Skip if "html_filter" processor disabled.
    }

    return $properties;
  }

  /**
   * Converting search_api base field type to elasticsearch field.
   *
   * @param string $source
   *   The source field type.
   *
   * @return mixed|string
   *   The convert field type.
   */
  public function fieldType(string $source) {
    $assoc = $this->associateTypes();

    return isset($assoc[$source]) ? $assoc[$source] : $source;
  }

  /**
   * Returns associate field types.
   *
   * @return array
   *   The associate SearchApi field types and elasticsearch field types.
   */
  public function associateTypes() {
    return [
      'string' => 'keyword',
      'text' => 'text',
      'decimal' => 'double',
    ];
  }

  /**
   * Returns new field name from source field name and boost value.
   *
   * @param string $field
   *   The source field name.
   * @param string $boost
   *   The field bool value.
   *
   * @return string
   *   The new field name.
   */
  public static function getTokenField($field, $boost) {
    $tag_str = preg_replace('@[^a-z0-9_.]+@', '_', $boost);

    return "{$field}_{$tag_str}";
  }

  /**
   * Returns enabled analyzers from index.
   *
   * @return array
   *   The enable analyzers.
   */
  protected function enabledAnalyzers() {
    $output = &drupal_static($this->index->id() . ':' . __FUNCTION__);
    if ($output) {
      return $output;
    }
    $output = array_filter($this->index->getThirdPartySetting('search_api_es', 'es_analyzer', []));
    return $output;
  }

}
