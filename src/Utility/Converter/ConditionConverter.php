<?php

namespace Drupal\search_api_es\Utility\Converter;

use Drupal\search_api\Query\ConditionGroupInterface;
use Drupal\search_api\Query\ConditionInterface;
use Drupal\search_api_es\Utility\Converter\Condition\Operator\BoolOperator;
use Drupal\search_api_es\Utility\Converter\Condition\Operator\RangeOperator;
use Drupal\search_api_es\Utility\Converter\Condition\Operator\TermOperator;
use Drupal\search_api_es\Utility\Converter\Condition\Operator\TermsOperator;

/**
 * Provides methods for converte query condition to elasticsearch filter.
 */
class ConditionConverter {

  /**
   * Converte query condition group to elasticsearch filter.
   *
   * @param \Drupal\search_api\Query\ConditionGroupInterface $conditionGroup
   *   The query condition group.
   *
   * @return array
   *   The elasticsearch filter.
   */
  public static function convertGroup(ConditionGroupInterface $conditionGroup): array {
    $conjuction = $conditionGroup->getConjunction() == 'OR' ? 'should' : 'filter';
    $output[$conjuction] = [];
    foreach ($conditionGroup->getConditions() as $condition) {
      if ($condition instanceof ConditionGroupInterface) {
        $result = self::convertGroup($condition);
        if (!empty($result)) {
          $output[$conjuction][]['bool'] = $result;
        }
        continue;
      }
      if (!$condition instanceof ConditionInterface) {
        continue;
      }

      $output[$conjuction][] = self::convert($condition);
    }

    return empty($output[$conjuction]) ? [] : $output;
  }

  /**
   * Converte query condition to elasticsearch filter.
   *
   * @param \Drupal\search_api\Query\ConditionInterface $condition
   *   The query condition.
   *
   * @return array
   *   The elasticsearch filter.
   */
  public static function convert(ConditionInterface $condition): array {
    $output = [];
    switch ($condition->getOperator()) {
      case '=':
        $output = (new TermOperator($condition->getField(), $condition->getValue()))->get();
        break;

      case 'IN':
        $output = (new TermsOperator($condition->getField(), $condition->getValue()))->get();
        break;

      case '<>':
        $output = (new BoolOperator($condition->getField(), $condition->getValue()))->get();
        break;

      case '>':
      case '>=':
      case '<':
      case '<=':
      case 'BETWEEN':
      case 'NOT BETWEEN':
        $output = (new RangeOperator($condition->getField(), $condition->getValue(), $condition->getOperator()))->get();
        break;
    }

    return $output;
  }

}
