<?php

namespace Drupal\search_api_es\Utility\Converter;

use Drupal\Component\Utility\NestedArray;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\SearchApiException;

/**
 * Defines object for convert SearchApi query to elasticseach DSL query.
 */
class QueryConverter {

  /**
   * This result data.
   *
   * @var array
   */
  protected $body;

  /**
   * The origin query.
   *
   * @var \Drupal\search_api\Query\QueryInterface
   */
  protected $query;

  /**
   * This "html_filter" configuration.
   *
   * @var array
   */
  protected $htmlFilterConfig = [];

  /**
   * QueryConverter constructor.
   *
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   The SearchApi query.
   */
  public function __construct(QueryInterface $query) {
    $this->query = $query;

    try {
      $this->html_filter_config = $query->getIndex()->getProcessor('html_filter')->getConfiguration();
    }
    catch (SearchApiException $e) {
      // Skip if "html_filter" processor disabled.
    }

  }

  /**
   * Returns new instance QueryConverter object.
   *
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   The SearchApi query.
   *
   * @return static
   */
  public static function create(QueryInterface $query) {
    return new static($query);
  }

  /**
   * Returns convert data.
   *
   * @return array
   *   The convert query body.
   *
   * @throws \Drupal\search_api\SearchApiException
   */
  public function toArray() {
    $this
      ->setOffset()
      ->setLimit()
      ->setSort()
      ->setCondition()
      ->setFulltext()
      ->setSpellCheck()
      ->setMlt()
      ->setFacets()
      ->setLanguage()
      ->setHighlight();

    return $this->body;
  }

  /**
   * Set language filter.
   *
   * @return $this
   */
  protected function setLanguage() {
    $language_ids = $this->query->getLanguages();

    if (!$language_ids) {
      return $this;
    }

    $filters = [];

    foreach ($language_ids as $id) {
      $filters['filter'][]['bool']['filter']['term']['search_api_language'] = $id;
    }

    if (!empty($filters)) {
      $this->addBool($filters);
    }

    return $this;
  }

  /**
   * Returns suggest parameters.
   *
   * @param string $prefix
   *   The suggest key prefix.
   * @param string $keys
   *   The keys for suggest query.
   * @param int $size
   *   The number of candidates.
   *   That are generated for each individual query term.
   * @param int $max_errors
   *   The maximum percentage of the terms
   *   considered to be misspellings in order to form a correction.
   * @param int $min_word_length
   *   The minimum length a suggest text term must have in order to be included.
   *
   * @return array
   *   The suggest property.
   *
   * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-suggesters.html#phrase-suggester
   */
  public function buildSuggest($prefix, $keys, $size, $max_errors = 3, $min_word_length = 3) {
    $suggests = [];

    $suggest_fields = [];
    $html_filter_fields = $this->getHtmlFilterFields();
    if (is_array($this->query->getFulltextFields())) {
      foreach ($this->query->getFulltextFields() as $fulltextField) {
        // Add raw field name if field not used in HTML-filter.
        if (!in_array($fulltextField, $html_filter_fields)) {
          $suggest_fields[] = $fulltextField;
          continue;
        }

        foreach ($this->getHtmlFilterTags() as $boost) {
          $suggest_fields[] = $fulltextField . '_' . $boost;
        }
      }
    }

    foreach ($suggest_fields as $suggest_field) {
      $suggest_key = $prefix . $suggest_field;
      $suggest = [
        'text' => $keys,
        'phrase' => [
          'field' => $suggest_field,
          'size' => $size,
          'max_errors' => $max_errors,
          'direct_generator' => [
            [
              'field' => $suggest_field,
              'min_word_length' => $min_word_length,
              'suggest_mode' => 'always',
            ],
          ],
        ],
      ];
      $suggests[$suggest_key] = $suggest;
    }

    return $suggests;
  }

  /**
   * Add facet parameters.
   *
   * @return $this
   */
  protected function setFacets() {
    $options = $this->query->getOption('search_api_facets');
    // Skip if option not exist.
    if (is_null($options)) {
      return $this;
    }

    $aggs = [];
    foreach ($options as $option) {
      $aggs[$option['field']] = [
        'terms' => [
          'field' => $option['field'],
          'size' => $option['limit'] ?: 10000,
          'min_doc_count' => $option['min_count'],
        ],
      ];

    }
    $this->body['aggs'] = $aggs;

    return $this;
  }

  /**
   * Add spellCheck parameters.
   *
   * @return $this
   */
  protected function setSpellCheck() {
    $options = $this->query->getOption('search_api_spellcheck');
    // Skip if option not exist.
    if (is_null($options)) {
      return $this;
    }
    $keys = $this->query->getKeys();
    if (is_array($keys)) {
      unset($keys['#conjunction']);
      unset($keys['#negation']);
      $keys = implode(' ', $keys);
    }

    $max_errors = isset($options['max_errors']) ? $options['max_errors'] : 3;
    $min_word_length = isset($options['min_word_length']) ? $options['min_word_length'] : 3;
    $suggest = self::buildSuggest('search_api_spellcheck_', $keys, 1, $max_errors, $min_word_length);
    if (!empty($suggest)) {
      $this->body['suggest'] = $suggest;
    }

    return $this;
  }

  /**
   * Add highlight parameter.
   *
   * @return $this
   */
  protected function setHighlight() {

    try {
      $highlight_config = $this->query->getIndex()->getProcessor('highlight')->getConfiguration();
      if (!empty($highlight_config['exclude_fields'])) {
        foreach ($highlight_config['exclude_fields'] as $exclude_field) {
          $this->body['highlight']['fields'][$exclude_field] = [
            'pre_tags' => $highlight_config['prefix'],
            'post_tags' => $highlight_config['suffix'],
            'fragment_size' => $highlight_config['excerpt_length'],
          ];
        }
      }
    }
    catch (SearchApiException $e) {
      // Skip configuration if "highlight" processor disable.
    }

    return $this;
  }

  /**
   * Add mlt query.
   *
   * @return $this
   */
  protected function setMlt() {
    if ($mlt_options = $this->query->getOption('search_api_mlt')) {
      $this->body['query']['bool']['must'][] = [
        'more_like_this' => MltConverter::create($mlt_options)->get(),
      ];
    }

    return $this;
  }

  /**
   * Add fulltext query parameters.
   *
   * @return $this
   *
   * @throws \Drupal\search_api\SearchApiException
   */
  protected function setFulltext() {

    $fulltext_fields = [];
    $html_filter_fields = $this->getHtmlFilterFields();
    $query_fulltext = is_array($this->query->getFulltextFields()) ?
      $this->query->getFulltextFields() : $this->query->getIndex()->getFulltextFields();
    foreach ($query_fulltext as $fulltextField) {
      $fulltext_fields[] = [
        // Add "_" suffix if field use in html_filter.
        'name' => in_array($fulltextField, $html_filter_fields) ? "{$fulltextField}_" : $fulltextField,
        'boost' => $this->query->getIndex()->getField($fulltextField)->getBoost(),
      ];
    }
    $query_parse_mode = $this->query->getParseMode()->getPluginId();
    $result = FullTextConverter::create()
      ->setKeys($this->query->getKeys())
      ->setParseMode($query_parse_mode)
      ->setFields($fulltext_fields)
      ->get();
    $this->addBool($result);

    return $this;
  }

  /**
   * Returns fields form "html_filter" preprocessor.
   *
   * @return array
   *   The html_filter fields.
   */
  protected function getHtmlFilterFields() {
    return isset($this->html_filter_config['fields']) ? $this->html_filter_config['fields'] : [];
  }

  /**
   * Returns tags form "html_filter" preprocessor.
   *
   * @return array
   *   The html_filter tags.
   */
  protected function getHtmlFilterTags() {
    return isset($this->html_filter_config['tags']) ? $this->html_filter_config['tags'] : [];
  }

  /**
   * Convert query condition.
   *
   * @return $this
   */
  protected function setCondition() {
    $conditions = ConditionConverter::convertGroup($this->query->getConditionGroup());
    $this->addBool($conditions);

    return $this;
  }

  /**
   * Set "size" parameter.
   *
   * @return $this
   */
  protected function setSort() {
    $sorts = $this->query->getSorts();

    if (array_key_exists('search_api_random', $sorts)) {
      $this->addBool([
        'should' => [
          [
            'function_score' => [
              'query' => ['match_all' => (object) []],
              'random_score' => (object) [],
              'boost_mode' => 'multiply',
            ],
          ],
        ],
      ]);
      unset($sorts['search_api_random']);
    }

    $this->body['sort'] = $sorts;

    return $this;
  }

  /**
   * Convert "limit" options to "size" parameter.
   *
   * @return $this
   */
  protected function setLimit() {
    $this->body['size'] = $this->query->getOption('limit', 10);

    return $this;
  }

  /**
   * Convert "offset" options to "from" parameter.
   *
   * @return $this
   */
  protected function setOffset() {
    $this->body['from'] = $this->query->getOption('offset', 0);

    return $this;
  }

  /**
   * Add conditions in "bool" parameter.
   *
   * @param array $conditions
   *   New condition for add in query filter.
   */
  protected function addBool(array $conditions) {
    // Merge if parameter array.
    if (isset($this->body['query']['bool']) && is_array($this->body['query']['bool'])) {
      $this->body['query']['bool'] = NestedArray::mergeDeep($this->body['query']['bool'], $conditions);

      return;
    }
    if (!empty($conditions)) {
      $this->body['query']['bool'] = $conditions;
    }
  }

}
