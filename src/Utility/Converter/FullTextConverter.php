<?php

namespace Drupal\search_api_es\Utility\Converter;

use Drupal\search_api\SearchApiException;

/**
 * Defines object for convert fulltext keys.
 */
class FullTextConverter {

  /**
   * This key for fulltext search.
   *
   * @var array|string|null
   */
  protected $keys;

  /**
   * This fields array for search keys.
   *
   * @var array
   */
  protected $fields;

  /**
   * This search mode for search keys.
   *
   * @var string
   */
  protected $parseModeId = 'direct';

  /**
   * This conjunction.
   *
   * @var string
   */
  protected $conjunction;

  /**
   * This negation.
   *
   * @var bool
   */
  protected $negation = FALSE;

  /**
   * Returns instance FullTextConverter object.
   *
   * @return static
   *   The FullTextConverter object.
   */
  public static function create() {
    return new static();
  }

  /**
   * Set search keys, conjunction and negation values.
   *
   * @param array $keys
   *   The fulltext keys.
   *
   * @return $this
   */
  public function setKeys($keys) {

    if (isset($keys['#conjunction'])) {
      $this->conjunction = $keys['#conjunction'];
      unset($keys['#conjunction']);
    }

    if (isset($this->keys['#negation'])) {
      $this->negation = $this->keys['#negation'];
      unset($keys['#negation']);
    }
    $this->keys = $keys;

    return $this;
  }

  /**
   * Set search field after convert.
   *
   * @param array $fields
   *   The fulltext fields.
   *
   * @return $this
   *
   * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-simple-query-string-query.html#simple-query-string-boost
   */
  public function setFields(array $fields) {
    foreach ($fields as $field) {
      if (is_string($field)) {
        $this->fields[] = "$field*";
      }
      if (isset($field['name'])) {
        $this->fields[] = isset($field['boost']) ? "{$field['name']}*^{$field['boost']}" : "{$field['name']}*";
      }
    }

    return $this;
  }

  /**
   * Set search parse mode.
   *
   * @param string $parse_mode_id
   *   The new parse mode.
   *
   * @return $this
   */
  public function setParseMode($parse_mode_id) {
    $this->parseModeId = $parse_mode_id;

    return $this;
  }

  /**
   * Returns convert FullTextQuery to DSL query.
   *
   * @return array
   *   The elasticsearch fulltext query.
   *
   * @throws \Drupal\search_api\SearchApiException
   */
  public function get() {
    if (is_null($this->keys)) {
      return [];
    }

    switch ($this->parseModeId) {
      case 'direct':
      case 'terms':
        return $this->multiMatch(FALSE, TRUE);

      case 'phrase':
        return $this->multiMatch(TRUE);

      case 'simple_query_string':
        return $this->simpleQueryString();

      default:
        throw new SearchApiException(sprintf('Parse mode %s requires fields.', $this->parse_mode_id));
    }

  }

  /**
   * Returns DSL array for "multi_match" query.
   *
   * @param bool $phrase
   *   The phrase status.
   * @param bool $fuzziness
   *   The fuzziness status.
   *
   * @return array
   *   The DSL array.
   *
   * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-multi-match-query.html
   */
  protected function multiMatch($phrase = FALSE, $fuzziness = FALSE) {
    $output = [
      'should' => [
        [
          'multi_match' => [
            'query' => is_array($this->keys) ? implode(' ', $this->keys) : $this->keys,
            'fields' => $phrase ? [] : $this->fields,
            'operator' => $this->conjunction,
            'type' => $phrase ? 'phrase_prefix' : 'best_fields',
          ],
        ],
      ],
    ];
    // Add fuzziness options.
    if ($fuzziness) {
      $output['should'][] = [
        'multi_match' => [
          'query' => is_array($this->keys) ? implode(' ', $this->keys) : $this->keys,
          'fields' => $phrase ? [] : $this->fields,
          'fuzziness' => 'AUTO',
          'type' => $phrase ? 'phrase_prefix' : 'best_fields',
        ],
      ];
    }

    return $output;
  }

  /**
   * Returns DSL array for "simple_query_string" query.
   *
   * @return array
   *   The DSL array.
   */
  protected function simpleQueryString() {
    return [
      $this->queryType() => [
        [
          'simple_query_string' => [
            'query' => $this->keys,
            'fields' => $this->fields,
            'minimum_should_match' => '50%',
          ],
        ],
      ],
    ];
  }

  /**
   * Set find type "must" or "must_not".
   *
   * @return string
   *   The search type.
   *
   * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-bool-query.html
   */
  protected function queryType() {
    return $this->negation ? 'must_not' : 'must';
  }

}
