<?php

namespace Drupal\search_api_es\Utility\Converter\Condition\Operator;

class TermsOperator extends OperatorBase {

  /**
   * @inheritDoc
   */
  public function get() {
    return [
      'terms' => [$this->field_name => array_values($this->field_value)],
    ];
  }
}
