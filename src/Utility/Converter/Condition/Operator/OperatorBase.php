<?php

namespace Drupal\search_api_es\Utility\Converter\Condition\Operator;

abstract class OperatorBase implements OperatorInterface {

  /**
   * @var string
   */
  protected $field_name;

  /**
   * @var mixed
   */
  protected $field_value;

  /**
   * Term constructor.
   *
   * @param string $field_name
   * @param mixed $field_value
   */
  public function __construct(string $field_name, $field_value) {
    $this->field_name = $field_name;
    $this->field_value = $field_value;
  }

}
