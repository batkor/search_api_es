<?php

namespace Drupal\search_api_es\Utility\Converter\Condition\Operator;

class BoolOperator extends TermOperator{

  /**
   * {@inheritdoc}
   */
  public function get() {
    return [
      'bool' => [
        'must_not' => parent::get(),
      ],
    ];
  }
}
