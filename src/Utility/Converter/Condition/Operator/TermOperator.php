<?php

namespace Drupal\search_api_es\Utility\Converter\Condition\Operator;

class TermOperator extends OperatorBase {

  /**
   * {@inheritdoc}
   */
  public function get() {
    return [
      'term' => [$this->field_name => $this->field_value],
    ];
  }

}
