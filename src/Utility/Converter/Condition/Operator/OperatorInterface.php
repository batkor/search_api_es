<?php

namespace Drupal\search_api_es\Utility\Converter\Condition\Operator;

interface OperatorInterface {

  /**
   * Returns array for Elasticsearch filter.
   *
   * @return array
   */
  public function get();

}
