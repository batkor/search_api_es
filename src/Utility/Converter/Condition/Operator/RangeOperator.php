<?php

namespace Drupal\search_api_es\Utility\Converter\Condition\Operator;

class RangeOperator extends OperatorBase implements RangeOperatorInterface {

  /**
   * This range operator.
   *
   * @var string
   */
  protected $operator;

  /**
   * RangeOperator constructor.
   *
   * @param string $field_name
   * @param mixed $field_value
   * @param string $operator
   */
  public function __construct(string $field_name, $field_value, string $operator) {
    parent::__construct($field_name, $field_value);
    $this->operator = $operator;
  }

  /**
   * @inheritDoc
   */
  public function get() {
    if ($this->operator == 'NOT BETWEEN') {
      return [
        'bool' => [
          'must_not' => [
            'range' => $this->buildRange()
          ],
        ],
      ];
    }
    return [
      'range' => $this->buildRange()
    ];
  }

  protected function buildRange() {
    return [
      $this->field_name => [
        'from' => $this->from(),
        'to' => $this->to(),
        'include_lower' => $this->include(),
        'include_upper' => $this->include(RangeOperatorInterface::UPPER_OPERATOR),
      ],
    ];
  }

  /**
   * Returns "from" key value.
   *
   * @return mixed|null
   */
  protected function from() {
    if (strripos($this->operator, '>') !== FALSE) {
      return $this->field_value;
    }

    if ($this->operator == 'BETWEEN' && is_array($this->field_value)) {
      return isset($this->field_value['0']) ? $this->field_value['0'] : NULL;
    }

    return NULL;
  }

  /**
   * Returns "to" key value.
   *
   * @return mixed|null
   */
  protected function to() {
    if (strripos($this->operator, '<') !== FALSE) {
      return $this->field_value;
    }

    if ($this->operator == 'BETWEEN' && is_array($this->field_value)) {
      return isset($this->field_value['1']) ? $this->field_value['1'] : NULL;
    }

    return NULL;
  }

  /**
   * Returns include status for "lower" and "upper" key values.
   *
   * @param string $operator
   *
   * @return bool
   */
  protected function include($operator = RangeOperatorInterface::LOWER_OPERATOR) {
    return $this->operator == $operator;
  }

}
