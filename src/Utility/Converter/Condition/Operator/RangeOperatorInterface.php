<?php

namespace Drupal\search_api_es\Utility\Converter\Condition\Operator;

interface RangeOperatorInterface {

  /**
   * Include "lower" if the operator is ">=".
   */
  const LOWER_OPERATOR = '>=';

  /**
   * Include "lower" if the operator is "<=".
   */
  const UPPER_OPERATOR = '<=';


}
