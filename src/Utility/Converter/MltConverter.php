<?php

namespace Drupal\search_api_es\Utility\Converter;

/**
 * Provider MLT query converter.
 *
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-mlt-query.html
 *
 * @package Drupal\search_api_es\Utility\Converter
 */
class MltConverter {

  /**
   * Converted options.
   *
   * @var array
   */
  protected $options = [];

  /**
   * MltConverter constructor.
   *
   * @param array $options
   *   The MLT options.
   */
  public function __construct(array $options) {
    $this->options = $options;
  }

  /**
   * Create new MLT converter.
   *
   * @param array $options
   *   The MLT options.
   *
   * @return static
   *   The instance MltConverter object.
   */
  public static function create(array $options) {
    return new static($options);
  }

  /**
   * Returns convert result.
   *
   * @return array
   *   The convert result.
   */
  public function get(): array {
    return $this->convert();
  }

  /**
   * Converting raw options.
   *
   * @return array
   *   The options.
   */
  protected function convert() {
    $this->options['like'] = $this->options['id'];
    unset($this->options['id']);
    $this->options['min_term_freq'] = 1;
    $this->options['min_doc_freq'] = 1;

    $this->options['fields'] = array_values($this->options['fields']);
    foreach ($this->options['fields'] as &$field) {
      $field .= ".mlt";
    }

    return $this->options;
  }

}
